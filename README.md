# node images

This project provides docker images for node. A scheduled pipeline runs daily to update the images.

Documentation: https://abfelbaum.dev/projects/list/images/dotnet
